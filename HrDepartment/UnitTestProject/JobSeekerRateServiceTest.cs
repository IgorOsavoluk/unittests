using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using HrDepartment.Application.Services;
using HrDepartment.Application.Interfaces;
using HrDepartment.Domain.Entities;
using HrDepartment.Domain.Enums;
using System.Threading.Tasks;
using System.Collections.Generic;
using System;

namespace UnitTestProject
{
    [TestClass]
    public class JobSeekerRateServiceTest
    {

        [DataTestMethod]
        [DataRow("2005,10,10", 15)]
        [DataRow("1992, 10, 10", 25)]
        [DataRow("1955, 10, 10", 15)]
        [DataRow("", 15)]
        [DataRow(null, 15)]
        [TestMethod]
        public void TestVariousBirthDates_ReturnRating(string testedBirthDate, int expectedRating)
        {
            DateTime targetBirthDate;

            //������� ��������� JobSeeker
            JobSeeker myjobseeker = new JobSeeker();

            myjobseeker.FirstName = "John";
            myjobseeker.MiddleName = "James";
            myjobseeker.LastName = "Trump";
            myjobseeker.Education = EducationLevel.None;
            myjobseeker.Experience = 0;
            myjobseeker.BadHabits = BadHabits.None;
            myjobseeker.Id = 1;
            myjobseeker.Status = JobSeekerStatus.New;

            //����������� ��������� JobSeeker
            if (DateTime.TryParse(testedBirthDate, out targetBirthDate))
                myjobseeker.BirthDate = targetBirthDate;

            var mockSanction = new Mock<ISanctionService>();
            var myRateService = new JobSeekerRateService(mockSanction.Object);

            int myresult = myRateService.CalculateJobSeekerRatingAsync(myjobseeker).Result;
            Assert.AreEqual(expectedRating, myresult);

        }
        [DataTestMethod]
        [DataRow(0, 25)]
        [DataRow(1, 30)]
        [DataRow(2, 40)]
        [DataRow(3, 60)]
        [DataRow(null,25)]
        [TestMethod]
        public void TestVariousEducationLevel_ReturnRating( int testedEducationLevel, int expectedRating)
        {
            //������� ��������� JobSeeker
            JobSeeker myjobseeker = new JobSeeker();
            myjobseeker.FirstName = "John";
            myjobseeker.MiddleName = "James";
            myjobseeker.LastName = "Trump";
            myjobseeker.Experience = 0;
            myjobseeker.BadHabits = BadHabits.None;
            myjobseeker.Id = 1;
            myjobseeker.Status = JobSeekerStatus.New;
            myjobseeker.BirthDate = new System.DateTime(1992, 10, 10);

            //����������� ��������� JobSeeker
                switch (testedEducationLevel)
                {
                    case 0:
                        myjobseeker.Education = EducationLevel.None;
                        break;
                    case 1:
                        myjobseeker.Education = EducationLevel.School;
                        break;
                    case 2:
                        myjobseeker.Education = EducationLevel.College;
                        break;
                    case 3:
                        myjobseeker.Education = EducationLevel.University;
                        break;
            };          

            var mockSanction = new Mock<ISanctionService>();
            var myRateService = new JobSeekerRateService(mockSanction.Object);

            int myresult = myRateService.CalculateJobSeekerRatingAsync(myjobseeker).Result;
            Assert.AreEqual(expectedRating, myresult);
        }
        [ExpectedException(typeof(AggregateException), "Exception �� ��������")]
        [DataRow(-1)]
        [DataRow(null)]
        [DataRow(0)]
        [DataRow(1)]
        [DataRow(int.MinValue)]
        [DataRow(int.MaxValue)]
        [TestMethod]
        public void TestOutRangeEducationLevel_ReturnException(int testedEducationLevel)
        {
            //������� ��������� JobSeeker
            JobSeeker myjobseeker = new JobSeeker();
            myjobseeker.FirstName = "John";
            myjobseeker.MiddleName = "James";
            myjobseeker.LastName = "Trump";
            myjobseeker.Experience = 0;
            myjobseeker.BadHabits = BadHabits.None;
            myjobseeker.Id = 1;
            myjobseeker.Status = JobSeekerStatus.New;
            myjobseeker.BirthDate = new System.DateTime(1992, 10, 10);

            //����������� ��������� JobSeeker
            if (testedEducationLevel < 0)
                myjobseeker.Education = myjobseeker.Education - 1;
            else
                myjobseeker.Education = EducationLevel.None;


            var mockSanction = new Mock<ISanctionService>();
            var myRateService = new JobSeekerRateService(mockSanction.Object);

            int myresult = myRateService.CalculateJobSeekerRatingAsync(myjobseeker).Result;
            throw new AggregateException("�������� exception Aggregate ����� ������ CalculateJobSeekerRatingAsync");

            // Assert.AreEqual(expectedRating, myresult);
        }



        [DataTestMethod]
        [DataRow(0, 25)]
        [DataRow(2, 30)]
        [DataRow(4, 35)]
        [DataRow(7, 45)]
        [DataRow(12, 60)]
        [TestMethod]
        public void TestVariousExperience_ReturnRating(int testedExperience, int expectedRating)
        {
            //������� ��������� JobSeeker
            JobSeeker myjobseeker = new JobSeeker();

            myjobseeker.FirstName = "John";
            myjobseeker.MiddleName = "James";
            myjobseeker.LastName = "Trump";
            myjobseeker.Education = EducationLevel.None;         
            myjobseeker.BadHabits = BadHabits.None;
            myjobseeker.Id = 1;
            myjobseeker.Status = JobSeekerStatus.New;
            myjobseeker.BirthDate = new System.DateTime(1992, 10, 10);

            //����������� ���������
            myjobseeker.Experience = testedExperience;

            var mockSanction = new Mock<ISanctionService>();
            var myRateService = new JobSeekerRateService(mockSanction.Object);

            int myresult = myRateService.CalculateJobSeekerRatingAsync(myjobseeker).Result;
            Assert.AreEqual(expectedRating, myresult);

        }
        [DataTestMethod]
        [DataRow(0, 25)]
        [DataRow(1, 20)]
        [DataRow(2, 10)]
        [DataRow(3, 0)]
        [DataRow(4, 0)]
        [TestMethod]
        public void TestVariousBadHabits_ReturnRating(int testedHabitSum, int expectedRating)
        {
            //������� ��������� JobSeeker
            JobSeeker myjobseeker = new JobSeeker();

            myjobseeker.FirstName = "John";
            myjobseeker.MiddleName = "James";
            myjobseeker.LastName = "Trump";
            myjobseeker.Education = EducationLevel.None;          
            myjobseeker.Id = 1;
            myjobseeker.Status = JobSeekerStatus.New;
            myjobseeker.BirthDate = new System.DateTime(1992, 10, 10);
            myjobseeker.Experience = 0;

            //����������� ���������
            switch (testedHabitSum)
            {
                case 0:
                    myjobseeker.BadHabits = BadHabits.None;
                    break;
                case 1:
                    myjobseeker.BadHabits = BadHabits.Smoking; 
                    break;
                case 2:
                    myjobseeker.BadHabits = BadHabits.Alcoholism;
                    break;
                case 3:
                    myjobseeker.BadHabits = BadHabits.Smoking | BadHabits.Alcoholism;
                    break;
                case 4:
                    myjobseeker.BadHabits = BadHabits.Drugs;
                    break;
            };

            var mockSanction = new Mock<ISanctionService>();
            var myRateService = new JobSeekerRateService(mockSanction.Object);

            int myresult = myRateService.CalculateJobSeekerRatingAsync(myjobseeker).Result;
            Assert.AreEqual(expectedRating, myresult);

        }
        [DataTestMethod]
        [DataRow(0, 25)]
        [DataRow(1, 25)]
        [DataRow(5, 25)]
        [DataRow(-1, 25)]
        [DataRow(100, 25)]
        [TestMethod]
        public void TestVariousID_ReturnRating(int testedID, int expectedRating)
        {
            //������� ��������� JobSeeker
            JobSeeker myjobseeker = new JobSeeker();

            myjobseeker.FirstName = "John";
            myjobseeker.MiddleName = "James";
            myjobseeker.LastName = "Trump";
            myjobseeker.Education = EducationLevel.None;
            myjobseeker.BadHabits = BadHabits.None;
            myjobseeker.Status = JobSeekerStatus.New;
            myjobseeker.BirthDate = new System.DateTime(1992, 10, 10);
            myjobseeker.Experience = 0;

            //����������� ���������
            if(testedID > 0)
                myjobseeker.Id = testedID;

            var mockSanction = new Mock<ISanctionService>();
            var myRateService = new JobSeekerRateService(mockSanction.Object);

            int myresult = myRateService.CalculateJobSeekerRatingAsync(myjobseeker).Result;
            Assert.AreEqual(expectedRating, myresult);

        }

        [TestMethod]
        public void TestSanctionListTrue_ReturnRating()
        {
            //������� ��������� JobSeeker
            JobSeeker myjobseeker = new JobSeeker();

            myjobseeker.Id = 1;
            myjobseeker.FirstName = "John";
            myjobseeker.MiddleName = "James";
            myjobseeker.LastName = "Trump";
            myjobseeker.Education = EducationLevel.None;
            myjobseeker.BadHabits = BadHabits.None;
            myjobseeker.Status = JobSeekerStatus.New;
            myjobseeker.BirthDate = new System.DateTime(1992, 10, 10);
            myjobseeker.Experience = 0;

            var mockSanction = new Mock<ISanctionService>();
            mockSanction.Setup(m => m.IsInSanctionsListAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<DateTime>())).ReturnsAsync(true);
            //����������� ���������


            var myRateService = new JobSeekerRateService(mockSanction.Object);

            int myresult = myRateService.CalculateJobSeekerRatingAsync(myjobseeker).Result;
            Assert.AreEqual(0, myresult);

        }
    }
}

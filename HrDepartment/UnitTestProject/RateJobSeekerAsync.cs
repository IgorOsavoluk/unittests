using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using AutoMapper;
using HrDepartment.Application.Services;
using HrDepartment.Application.Interfaces;
using HrDepartment.Application.Dto;
using HrDepartment.Domain.Entities;
using HrDepartment.Domain.Enums;
using HrDepartment.Infrastructure;
using HrDepartment.Infrastructure.Interfaces;
using System.Threading.Tasks;
using System.Collections.Generic;
using System;

namespace UnitTestProject
{
    [TestClass]
    public class RateJobSeekerAsync
    {
        [DataTestMethod]
        [DataRow(0)]
        [DataRow(1)]
        [DataRow(5)]
        [DataRow(-1)]
        [DataRow(100)]
        public void TestVariousRating_ReturnsExpectedRating(int expectedRating)
        {
            //������� ��������� JobSeeker
            JobSeeker myjobseeker = new JobSeeker();
            myjobseeker.FirstName = "John";
            myjobseeker.MiddleName = "James";
            myjobseeker.LastName = "Trump";
            myjobseeker.Education = EducationLevel.None;
            myjobseeker.Experience = 0;
            myjobseeker.BadHabits = BadHabits.None;
            myjobseeker.Id = 1;
            myjobseeker.Status = JobSeekerStatus.New;

            JobSeeker mynewjobseeker = new JobSeeker();

            //�������� �������
            var mockStorage = new Mock<IStorage>();
            var mockRateService = new Mock<IRateService<JobSeeker>>();
            var mockMapper = new Mock<IMapper>();
            var mockNotificationService = new Mock<INotificationService>();

            mockStorage.Setup(m => m.GetByIdAsync<JobSeeker, It.IsAnyType>(It.IsAny<It.IsAnyType>())).ReturnsAsync(myjobseeker);
            mockRateService.Setup(m => m.CalculateJobSeekerRatingAsync(myjobseeker)).ReturnsAsync(expectedRating);

            //������� ������
            var myJobSeekerService = new JobSeekerService(mockStorage.Object, mockRateService.Object, mockMapper.Object, mockNotificationService.Object);

            int actualRating = myJobSeekerService.RateJobSeekerAsync(myjobseeker.Id).Result;
            Assert.AreEqual(expectedRating, actualRating);

        }
    }
}
